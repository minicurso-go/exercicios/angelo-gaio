package main

import "fmt"

func piramide(altura int) {
	for i := 0; i < altura; i++ {
		for j := 0; j < altura-i-1; j++ {	// -1 para tirar o espaco extra
			fmt.Print(" ")	// Espacos na frente
		}
		for k := 0; k <= i; k++ {
			fmt.Print("* ") // asteriscos seguidos em uma linha
		}
		fmt.Println()
	}
}

func main() {
	var altura int

	fmt.Print("Digite a altura da piramide: ")
	fmt.Scanln(&altura)

	piramide(altura)
}
