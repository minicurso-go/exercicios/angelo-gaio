package main

import (
	"fmt"
	"math/big"
)

func main() {
	var i1, i2 int
	fmt.Print("Numero para o intervalo(n1 n2): ")
	fmt.Scan(&i1, &i2)

	var dist int
	if sub := i2 - i1; sub < 0 {
		dist = i1 - i2
	} else {
		dist = i2 - i1
	}

	array := make([]int, 0, dist)

	for i := int64(i1); i <= int64(i2); i++ {
		if big.NewInt(i).ProbablyPrime(0) {
			array = append(array, int(i))
		}
	}

	fmt.Print("Array de primo: ", array)
}
