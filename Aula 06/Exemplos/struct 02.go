package main

import (
	"fmt"
)

type autenticacao struct {
	user string
	pass string
}

func (data *autenticacao) verUsuarios() { // ponteiro para garantir que a struct seja alterada
	fmt.Printf("Usuario: %s, Senha: %s", data.user, data.pass)
}

func (data *autenticacao) addUsuario(a, b string) { // ponteiro para garantir que a struct seja alterada
	data.user = a
	data.pass = b
}

func main() {
	var a, b string

	fmt.Scan(&a, &b)
	fmt.Println()

	data := autenticacao{}
	data.addUsuario(a, b)
	data.verUsuarios()
}
