package main

import (
	"fmt"
)

func main() {

	type rodas struct {
		Aro  int
		pneu string
	}

	type caro struct {
		Marca      string
		Modelo     string
		Ano        int
		RodaFrente rodas
		RodaTras   rodas
	}

	meuCarro := caro{}
	meuCarro.Marca = "Fiat"
	meuCarro.RodaFrente.Aro = 15

	fmt.Println("Marca:", meuCarro.Marca)
	fmt.Println("Aro:", meuCarro.RodaFrente.Aro)
}
