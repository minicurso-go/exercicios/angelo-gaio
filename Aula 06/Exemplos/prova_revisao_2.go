package main

import (
	"fmt"
	"strconv"
)

func fatorial(x int) int {
	if x == 0 {
		return 1
	}
	return x * fatorial(x-1)
}

func main() {
	var input string

	fmt.Print("Digite um numero: ")
	fmt.Scan(&input)

	x, err := strconv.Atoi(input) // converte string para int
	if err != nil || x < 0 {
		fmt.Println("Numero invalido")
	}

	fmt.Println("Fatorial:", fatorial(x))
}
