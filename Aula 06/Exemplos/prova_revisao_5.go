package main

import (
	"fmt"
	"strings"
)

func piramide(altura int) {
	for i := 0; i < altura; i++ {
		fmt.Print(strings.Repeat(" ", altura-i-1))
		fmt.Print(strings.Repeat("*", 2*i+1))
		fmt.Println()
	}
}
func main() {
	var altura int

	fmt.Print("Digite a altura da piramide: ")
	fmt.Scanln(&altura)

	piramide(altura)
}
