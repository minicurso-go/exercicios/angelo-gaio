package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	var input string
	fmt.Print("Digite a sua operacao: ")
	fmt.Scanln(&input)

	var partes []string
	var operador string

	if strings.Contains(input, "+") {
		partes = strings.Split(input, "+")
		operador = "+"
	} else if strings.Contains(input, "-") {
		partes = strings.Split(input, "-")
		operador = "-"
	} else if strings.Contains(input, "*") {
		partes = strings.Split(input, "*")
		operador = "*"
	} else if strings.Contains(input, "/") {
		partes = strings.Split(input, "/")
		operador = "/"
	} else {
		fmt.Println("Operacao invalida")
		return
	}

	if len(partes) != 2 {
		fmt.Println("Operacao invalida")
		return
	}

	x, err1 := strconv.ParseFloat(partes[0], 64)
	y, err2 := strconv.ParseFloat(partes[1], 64)
	if err1 != nil || err2 != nil {
		fmt.Println("Numeros invalidos")
		return
	}

	switch operador {
	case "+":
		fmt.Println("Resultado:", x+y)
	case "-":
		fmt.Println("Resultado:", x-y)
	case "*":
		fmt.Println("Resultado:", x*y)
	case "/":
		if y == 0 {
			fmt.Println("Nao e possivel dividir por 0")
			return
		}
		fmt.Println("Resultado:", x/y)
	default:
		fmt.Println("Operador invalido")
	}

}
