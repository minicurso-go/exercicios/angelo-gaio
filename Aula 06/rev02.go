package main

import (
	"fmt"
)

type funcionario struct {
	nome    string
	salario float32
	idade   int
}

func (data *funcionario) alterarSalario(y float32) float32 {
	var porc float32

	fmt.Print("Digite a porcentagem (1.2 = 20%): ")
	fmt.Scan(&porc)
	return y * porc
}

func (data *funcionario) tempoServico(i int) int {
	data.idade = i
	return i - 18
}

func main() {
	var x string
	var y float32
	var z int
	funci := funcionario{}

	fmt.Print("Digite o nome do funcionario: ")
	fmt.Scan(&x)
	fmt.Print("Digite o salario: ")
	fmt.Scan(&y)
	fmt.Print("Digite a idade: ")
	fmt.Scan(&z)

	fmt.Println()
	fmt.Printf("\nNovo salario: %v", funci.alterarSalario(y))
	fmt.Printf("\nTempo de trabalho: %v anos.", funci.tempoServico(z))
}
