package main

import "fmt"

type triangulo struct {
	base   float32
	altura float32
}

func main() {
	var x, y float32
	triang := triangulo{}

	fmt.Print("Digite o valor da base: ")
	fmt.Scan(&x)
	fmt.Print("Digite o valor da altura: ")
	fmt.Scan(&y)
	fmt.Println("Area:", triang.area(x, y))
}

func (val *triangulo) area(x, y float32) float32 {
	val.base = x
	val.altura = y
	return x * y / 2
}
