package main

import (
	"fmt"
)

type animal struct {
	nome,
	especie,
	som string
	idade int
}

func (data *animal) mudarSom() {
	var novoSom string

	fmt.Print("Digite o som do animal: ")
	fmt.Scan(&novoSom)
	data.som = novoSom
}

func (data *animal) info() {
	fmt.Println("Nome:", data.nome, "\nEspecie:", data.especie, "\nIdade:", data.idade, "\nSom:", data.som)
}

func main() {
	ani := animal{}
	ani.nome = "Theo"
	ani.idade = 8
	ani.especie = "cachorro"
	ani.mudarSom()
	ani.info()
}
