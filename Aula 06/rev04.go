package main

import (
	"fmt"
)

type Viagem struct {
	origem  string
	destino string
	data    string
	preco   float32
}

func maisCara(viagens []Viagem) Viagem {
	var maisCara Viagem
	for _, viagem := range viagens {
		if viagem.preco > maisCara.preco {
			maisCara = viagem
		}
	}
	return maisCara // retorna a struct completa com a viagem mais cara
}

func main() {
	viagens := []Viagem{
		{"Brasilia", "Rio de Janeiro", "2024-04-16", 1399.9},
		{"Rio de Janeiro", "São Paulo", "2024-04-22", 1185.0},
		{"São Paulo", "Brasilia", "2024-04-28", 999.9},
		{"Brasilia", "Belo Horizonte", "2024-05-01", 1565.5},
	}

	cara := maisCara(viagens)
	fmt.Println("Viagem mais cara:", cara.origem, "-", cara.destino, cara.data, "\nPreco:", cara.preco)
}
