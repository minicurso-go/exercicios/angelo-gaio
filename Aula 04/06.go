package main

import (
	"fmt"
	"math"
)

func pot(num, elev float64) float64 {
	res := math.Pow(num, elev)
	return res
}

func main() {
	var num, elev float64

	fmt.Print("Insira um numero: ")
	fmt.Scan(&num)
	fmt.Print("Elevar a: ")
	fmt.Scan(&elev)

	fmt.Println()
	fmt.Printf("Numero elevado %.2f", pot(num, elev))
}
