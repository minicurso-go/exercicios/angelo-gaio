package main

import "fmt"

func main() {
	var x, y int

	fmt.Print("Digite dois valoes (x y): ")
	fmt.Scan(&x, &y)

	fmt.Println()
	sum(x, y)
}

func sum(x, y int) {
	fmt.Println(x + y)
}
