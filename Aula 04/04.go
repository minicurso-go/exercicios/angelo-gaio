package main

import "fmt"

func fact(n int) int {
	if n <= 1 {
		return 1
	}
	return n * fact(n-1)
}

func main() {
    var n int
    
	fmt.Print("Digite um numero: ")
	fmt.Scan(&n)
	
	fmt.Println()
	fmt.Printf("O fatorial de e: %d", fact(n))
}
