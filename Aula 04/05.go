package main

import (
	"fmt"
	"math/rand"
)

func procura(lista []int, alvo int) bool {
	for _, elemento := range lista { //loop que descarta o indice e procura no
		if elemento == alvo { //range da lista por um elemento igual a alvo
			return true
		}
	}
	return false
}

func main() {
	var alvo int
	var lista []int

	for i := 0; i < 10; i++ {
		numRandom := rand.Intn(90) + 10 //gera numeros aleatorios de 0 a 90 e soma 10
		lista = append(lista, numRandom)
	}

	fmt.Print("Chute um numero de dois digitos: ")
	fmt.Scan(&alvo)
	fmt.Println()
	fmt.Print("Lista: ", lista)
	fmt.Println()

	if acertou := procura(lista, alvo); acertou { //define acertou como o resultado booleano da func
		fmt.Printf("%v esta na lista", alvo)
	} else {
		fmt.Printf("%v nao está na lista", alvo)
	}
}
