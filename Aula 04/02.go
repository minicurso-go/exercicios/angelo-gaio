package main

import "fmt"

func main() {
	var temp float32

	fmt.Print("Digite uma temperatura em c: ")
	fmt.Scan(&temp)

	fmt.Println()
	conv(temp)
}

func conv(temp float32) {
	fahrenheit := (temp * 1.8) + 32

	fmt.Printf("Temperatura em %.2f", fahrenheit)
}
