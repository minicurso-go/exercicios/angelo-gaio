package main

import "fmt"

func primo(num int) {
	if num < 1 {
		fmt.Println("Nao e primo")
		return
	}

	for i := 2; i < num; i++ {
		if num%i == 0 {
			fmt.Printf("Nao e primo")
			return
		}
	}
	fmt.Println("O numero e primo")
}

func main() {
	var num int

	fmt.Print("Insira um numero: ")
	fmt.Scan(&num)

	fmt.Println()
	primo(num)
}
