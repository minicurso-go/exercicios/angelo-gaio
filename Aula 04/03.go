package main

import "fmt"

func media(numeros []float64) float64 {
    total := 0.0
    for _, num := range numeros {
        total += num
    }
    return total / float64(len(numeros))
}

func main() {
    lista := []float64{11, 22, 33, 44, 55}
    
    media := media(lista)
    fmt.Printf("Media da lista: %.2f", media)
}
