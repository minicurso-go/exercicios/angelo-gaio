package main

import "fmt"

func main() {
	var A, B, C, D int

	fmt.Print("Digite o valor de A: ")
	fmt.Scanln(&A)
	fmt.Print("Digite o valor de B: ")
	fmt.Scanln(&B)
	fmt.Print("Digite o valor de C: ")
	fmt.Scanln(&C)
	fmt.Print("Digite o valor de D: ")
	fmt.Scanln(&D)

	fmt.Println()
	fmt.Printf("Resultado: %d", (A*B)-(C*D))
}
