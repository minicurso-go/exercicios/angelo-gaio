package main

import "fmt"

func main() {
	var val1, val2 float32
	var op string

	fmt.Println("Escolha dois valores (x y): ")
	fmt.Scan(&val1, &val2)
	fmt.Println()
	fmt.Println("Escolha a operacao (+, -, *, /): ")
	fmt.Scan(&op)

	switch op {
	case "+":
		fmt.Println(val1 + val2)
	case "-":
		fmt.Println(val1 - val2)
	case "*":
		fmt.Println(val1 * val2)
	case "/":
		if val2 == 0 {
			fmt.Println("Nao existe divisao por 0")
		} else {
			fmt.Println(val1 / val2)
		}
	default:
		fmt.Println("Operacao invalida!")
	}
}
