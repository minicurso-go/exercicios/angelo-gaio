package main

import (
	"fmt"
	"math/rand"
)

func main() {
	var array [10]float32
	var novo []float32

	for i := 0; i < len(array); i++ {
		numRandom := rand.Float32() * 10
		array[i] = numRandom
	}

	fmt.Printf("%.2v", array)
	fmt.Println()

	for e := 0; e < len(array); e++ {
		if array[e] > 5 {
			novo = append(novo, array[e])
		}
	}
	fmt.Printf("%.2v", novo)
}
