package main

import "fmt"

func main() {
	soma := 0
	tamanho := 0

	fmt.Print("Digite o tamanho do slice: ")
	fmt.Scan(&tamanho)

	slice := make([]int, tamanho)

	fmt.Println("Digite os valores: ")

	for i := range slice {
		fmt.Scan(&slice[i])
	}

	for _, fim := range slice {
		soma += fim
	}
	fmt.Println()
	fmt.Println(slice)
	fmt.Println(soma)
}
