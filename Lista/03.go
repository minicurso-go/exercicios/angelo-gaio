package main

import "fmt"

func main() {
	elementos := [4]float32{}
	var prod float32

	for _, cadaElemento := range elementos {
		prod *= cadaElemento
	}

	fmt.Println(prod)
}
