package main

import "fmt"

func main() {
	elementos := []int{1, 2, 3, 4, 5}

	elementos = append(elementos[:2], elementos[3:]...)

	fmt.Println(elementos)
}
