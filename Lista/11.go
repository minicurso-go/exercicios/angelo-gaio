package main

import (
	"fmt"
	"math/rand"
)

func main() {
	var adc int
	array := [7]int{}

	for i := 0; i < len(array); i++ {
		numRandom := rand.Intn(20)
		array[i] = numRandom
	}

	fmt.Println(array)
	fmt.Print("Numero a ser adcionado: ")
	fmt.Scan(&adc)
	fmt.Println()

	array[0] += adc
	array[len(array)-1] += adc

	fmt.Println("Array final:")
	fmt.Println(array)
}
