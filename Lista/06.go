package main

import (
	"fmt"
	"math/rand"
)

func adc(array [5]int, add int) {
	found := false

	for _, num := range array {
		if num == add {
			fmt.Println("O número já está presente.")
			fmt.Println(array)
			found = true
			break
		}
	}
	if !found {
		index := rand.Intn(len(array))
		array[index] = add

		fmt.Println("O número foi adicionado.")
		fmt.Println(array)
	}
}

func main() {
	var add int
	var array [5]int

	for i := 0; i < len(array); i++ {
		numRandom := rand.Intn(10)
		array[i] = numRandom
	}

	fmt.Print("Numero para adcionar: ")
	fmt.Scan(&add)

	adc(array, add)
}
