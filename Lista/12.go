package main

import (
	"fmt"
	"math/rand"
)

func main() {
	var troca1, troca2 int
	slice := make([]int, 8)

	for i := 0; i < len(slice); i++ {
		numRandom := rand.Intn(20)
		slice[i] = numRandom
	}

	fmt.Println(slice)
	fmt.Println("Indices a serem trocados (comecando de 1): ")
	fmt.Scan(&troca1, &troca2)
	fmt.Println()

	temp := slice[troca1-1]

	slice[troca1-1] = slice[troca2-1]
	slice[troca2-1] = temp

	fmt.Println("Resultado: ")
	fmt.Println(slice)
}
