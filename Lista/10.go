package main

import (
	"fmt"
	"math/rand"
)

func main() {
	array := [5]int{}
	var novo []int
	for i := 0; i < len(array); i++ {
		numRandom := rand.Intn(31)
		array[i] = numRandom
	}

	fmt.Println(array)
	fmt.Println()

	for e := 0; e < len(array); e++ {
		if array[e]%3 == 0 {
			novo = append(novo, array[e])
		}
	}
	fmt.Printf("Novo slice: %v", novo)
}
