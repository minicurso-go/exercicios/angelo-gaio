package main

import (
	"fmt"
	"math/rand"
)

func main() {
	var array [10]int
	var soma int

	for i := 0; i < len(array); i++ {
		numRandom := rand.Intn(10)
		array[i] = numRandom
	}

	fmt.Print(array)
	fmt.Println()

	for _, valor := range array {
		if valor%2 == 0 {
			soma += valor
		}
	}

	fmt.Print(soma)
}
