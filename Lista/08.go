package main

import (
	"fmt"
	"math/rand"
)

func main() {
	var add float32
	slice := make([]float32, 6)

	for i := 0; i < len(slice); i++ {
		numRandom := rand.Float32() * 10
		slice[i] = numRandom
	}

	fmt.Printf("%.3v\n", slice)
	fmt.Print("Numero para adcionar a todas as posicoes: ")
	fmt.Scan(&add)
	fmt.Println()

	for e := 0; e < len(slice); e++ {
		slice[e] += add
	}

	fmt.Printf("%.3v\n", slice)
}
