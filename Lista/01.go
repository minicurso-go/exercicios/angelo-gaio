package main

import "fmt"

func main() {
	elementos := [3]int{8, 15, 22}
	soma := 0

	for _, fim := range elementos {
		soma += fim
	}
	fmt.Println(soma)
}
