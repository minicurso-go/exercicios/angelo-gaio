package main

import (
	"fmt"
	"math/rand"
)

func procura(array [10]int, num int) bool {
	for _, elem := range array {
		if elem == num {
			return true
		}
	}
	return false
}

func main() {
	var num int
	var array [10]int

	for i := 0; i < 10; i++ {
		numRandom := rand.Intn(10)
		array[i] = numRandom
	}

	fmt.Print("Digite o valor a pesquisar: ")
	fmt.Scan(&num)
	fmt.Println()
	fmt.Print("Lista: ", array)
	fmt.Println()

	if acertou := procura(array, num); acertou { // Se acertou for true
		fmt.Printf("%v esta na lista", num)
	} else {
		fmt.Printf("%v nao está na lista", num)
	}
}
