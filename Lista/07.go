package main

import (
	"fmt"
	"math/rand"
)

func main() {
	var rem int
	slice := make([]int, 9)

	for i := 0; i <= 8; i++ {
		numRandom := rand.Intn(10)
		slice[i] = numRandom
	}

	fmt.Println(slice)
	fmt.Print("Numero para remover: ")
	fmt.Scan(&rem)
	fmt.Println()

	for e := 0; e < len(slice); e++ {
		if slice[e] == rem {
			slice = append(slice[:e], slice[e+1:]...)
			e--
		}
	}
	
	fmt.Println(slice)
}
