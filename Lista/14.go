package main

import (
	"fmt"
	"math/rand"
)

func main() {
	var array [10]int
	var novo []int

	for i := 0; i < len(array); i++ {
		numRandom := rand.Intn(10)
		array[i] = numRandom
	}

	fmt.Print(array)
	fmt.Println()

	for e := 0; e < len(array); e++ {
		if array[e]%2 == 0 {
			novo = append(novo, array[e])
		}
	}

	fmt.Print(novo)
}
