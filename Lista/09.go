package main

import (
	"fmt"
	"math"
	"math/rand"
)

func main() {
	slice := make([]int, 10)
	menor := math.MaxInt
	maior := -math.MaxInt
	for i := 0; i < len(slice); i++ {
		numRandom := rand.Intn(21)
		slice[i] = numRandom
	}

	fmt.Println(slice)
	fmt.Println()

	for e := 0; e < len(slice); e++ {
		if slice[e] < menor {
			menor = slice[e]
		} else if slice[e] > maior {
			maior = slice[e]
		}
	}
	fmt.Printf("Maior valor: %v", maior)
	fmt.Println()
	fmt.Printf("Menor valor: %v", menor)
}
