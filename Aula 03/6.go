package main

import "fmt"

func main() {
	var val int = -1
	var maior int

	for val != 0 {
		fmt.Print("Digite: ")
		fmt.Scan(&val)

		if val > maior {
			maior = val
		}
	}
	fmt.Println("O maior valor e:", maior)
}
