package main

import "fmt"

func main() {
	var num uint
	fmt.Print("Digite um numero: ")
	fmt.Scan(&num)

	for i := uint(1); i <= num; i++ {
		if num%i == 0 {
			fmt.Println(i)
		}
	}
}
